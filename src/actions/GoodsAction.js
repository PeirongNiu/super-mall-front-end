export const getAllGoods = () => (dispatch) => {
  fetch("http://localhost:8080/mall/goods")
    .then(response => response.json())
    .then(response => {
      dispatch({
        type: 'GET_ALL_GOODS',
        goods: response
      });
    })
};