import React, {Component} from 'react';
import {BrowserRouter as Router, NavLink} from "react-router-dom";
import {Route, Switch} from "react-router";
import 'antd/dist/antd.css';
import './App.less';
import home from "./pages/home";
import Header from "./compontents/header"
import Footer from './compontents/footer'
import createGoods from "./pages/createGoods";
import Order from "./pages/Order";

class App extends Component{
  render() {
    return (
      <Router>
        <Header/>
        <Switch>
          <Route path="/order" component={Order}></Route>
          <Route path="/create" component={createGoods}></Route>
          <Route path="/" component={home}></Route>
        </Switch>
        <Footer/>
      </Router>
    );
  }
}

export default App;