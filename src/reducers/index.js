import {combineReducers} from "redux";
import goodsReducer from "./goodsReducer";
import ordersReducer from "./ordersReducer";

const reducers = combineReducers({
  goodsReducer,
  ordersReducer
});
export default reducers;