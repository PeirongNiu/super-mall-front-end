const initState = {
  goods: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_ALL_GOODS':
      return{
        ...state,
        goods: action.goods
      };
    default:
      return state;
  }
}