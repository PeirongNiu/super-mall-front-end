import React, {Component} from 'react';
import {Modal} from 'antd';
import './styles/createGoods.less';

class CreateGoods extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      name: '',
      price: 0,
      unit: '',
      img: '',
      visible: false
    };
    this.handleName = this.handleInput.bind(this, 'name');
    this.handlePrice = this.handleInput.bind(this, 'price');
    this.handleUnit = this.handleInput.bind(this, 'unit');
    this.handleImg = this.handleInput.bind(this, 'img');
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOk = this.handleOk.bind(this);

  }

  handleInput(type, e) {
    this.setState({
      [type]: e.target.value
    })
  }

  handleOk() {
    this.setState({
      visible: false
    });
  };

  handleSubmit() {
    fetch('http://localhost:8080/mall/goods',
      {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          name: this.state.name,
          price: this.state.price,
          unit: this.state.unit,
          img: this.state.img
        })
      }).then(response => {
      if (response.status === 201) {
        this.props.history.push('/')
      }
      if (response.status === 400) {
        this.setState({
          visible: true
        })
      }
    }).catch((e) => {
      console.log(e);
    })
  }

  render() {
    let flag = false;
    if (this.state.name === '' || this.state.price === 0 || this.state.unit === '' || this.state.img === '')
      flag = true;
    return (
      <section className='create-goods'>
        <h2>添加商品</h2>
        <form>
          <div>
            <span><span className="form-required">*</span>名称：</span>
            <input type="text" placeholder='名称' onChange={this.handleName}/>
          </div>
          <div>
            <span><span className="form-required">*</span>价格：</span>
            <input type="text" placeholder='价格' onChange={this.handlePrice}/>
          </div>
          <div>
            <span><span className="form-required">*</span>单位：</span>
            <input type="text" placeholder='单位' onChange={this.handleUnit}/>
          </div>
          <div>
            <span><span className="form-required">*</span>图片：</span>
            <input type="text" placeholder='url' onChange={this.handleImg}/>
          </div>
        </form>
        <button onClick={this.handleSubmit} disabled={flag}>提交</button>
          <Modal
          visible={this.state.visible}
          onOk={this.handleOk}
          closable={false}
        >
          <p>商品名称已存在， 请输入新的商品名称</p>
        </Modal>
      </section>
    )
  }

}

export default CreateGoods;