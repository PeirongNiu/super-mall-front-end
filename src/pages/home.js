import React, {Component} from 'react';
import Goods from '../compontents/goods';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getAllGoods} from "../actions/GoodsAction";
import './styles/home.less'


class home extends Component{
  componentDidMount() {
    this.props.getAllGoods();
  }

  render() {
    const goods = this.props.goods;
    return(
      <section className='order-list'>
        {goods.map(value => {
          return <Goods key={value.goodsId} goodsId={value.goodsId} name={value.name} price={value.price} unit={value.unit} img={value.img}/>
        })}
      </section>
    );
  }
}

const mapStateToProps = state => ({
  goods: state.goodsReducer.goods
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllGoods
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(home);