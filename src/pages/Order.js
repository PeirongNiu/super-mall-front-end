import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getAllOrders} from "../actions/OrdersAction";
import "./styles/order.less";


class Order extends Component{

  componentDidMount() {
    this.props.getAllOrders();
  }

  handleDelete(orderId){
    fetch('http://localhost:8080/mall/orders/'+orderId,
      {method: 'delete'})
      .then(response=>{
        if (response.status==200)
          this.props.history.push('/')
      }).catch(e => console.log(e))
  }

  render() {
    const orders = this.props.orders;
    return(
      <section className='table'>
        <table cellSpacing='0' className='order-table'>
          <tr>
            <th>名称</th>
            <th>单价</th>
            <th>数量</th>
            <th>单位</th>
            <th>操作</th>
          </tr>
         {orders.map(order=>{
           return (
             <tr>
               <td>{order.name}</td>
               <td>{order.price}</td>
               <td>{order.num}</td>
               <td>{order.unit}</td>
               <td><button onClick={this.handleDelete.bind(this, order.orderId)}>删除</button></td>
             </tr>
           )
         })}
        </table>
      </section>
    );
  }
}
const mapStateToProps = state => ({
  orders: state.ordersReducer.orders
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllOrders
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Order);