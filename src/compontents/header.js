import {NavLink} from "react-router-dom";
import React from "react";
import {IoIosHome} from "react-icons/io";
import {FaShoppingCart} from "react-icons/fa";
import {MdControlPoint} from "react-icons/md";
import "./styles/header.less"

export default () => {
  return (
    <header className="menu">
      <ul>
        <li>
          <NavLink
            to="/"
            exact
            activeStyle={{
              backgroundColor: "rgb(63,165,247)"
            }}>
            <IoIosHome className="icon"/>商城
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/order"
            activeStyle={{
              backgroundColor: "rgb(63,165,247)"
            }}>
            <FaShoppingCart className="icon"/>订单
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/create"
            activeStyle={{
              backgroundColor: "rgb(63,165,247)"
            }}>
            <MdControlPoint className="icon"/>添加商品
          </NavLink>
        </li>
      </ul>
    </header>
  )
}