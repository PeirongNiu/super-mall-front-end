import React, {Component} from 'react';
import './styles/goods'

class Goods extends Component {

  handleClick() {
    fetch("http://localhost:8080/mall/orders/" + this.props.goodsId,
      {method: 'post'}).catch(e => console.log(e))
  }

  render() {
    return (
      <section className="goods">
        <div>
          <img
            src={this.props.img}/><br/>
          <h2 className="name">{this.props.name}</h2><br/>
          <h4 className="price">单价:{this.props.price}元/{this.props.unit}</h4>
          <button onClick={this.handleClick.bind(this)}>+</button>
        </div>
      </section>
    )
  }

}

export default Goods;